package com.example.domain.usecases

import com.example.domain.model.PostModel
import io.reactivex.Flowable

interface GetCacheFavoritePostsUseCase {
    fun getFavoritesFlow(): Flowable<List<PostModel>>
}