package com.example.domain.usecases

import com.example.domain.model.PostModel
import io.reactivex.Observable

interface GetPostsUseCase {
    fun getPostsObs(): Observable<List<PostModel>>
}