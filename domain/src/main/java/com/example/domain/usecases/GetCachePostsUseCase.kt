package com.example.domain.usecases

import com.example.domain.model.PostModel
import io.reactivex.Flowable

interface GetCachePostsUseCase {
    fun getPostsObs(): Flowable<List<PostModel>>
}