package com.example.domain.usecases

import com.example.domain.model.PostModel
import com.example.domain.repository.PostsDBRepo
import io.reactivex.Flowable

class GetCacheFavoritePostsUseCaseImpl(
    private val postsDBRepo: PostsDBRepo
) : GetCacheFavoritePostsUseCase {
    override fun getFavoritesFlow(): Flowable<List<PostModel>> {
        return postsDBRepo.getFavorites()
    }
}