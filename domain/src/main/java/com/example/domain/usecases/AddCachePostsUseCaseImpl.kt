package com.example.domain.usecases

import com.example.domain.model.PostModel
import com.example.domain.repository.PostsDBRepo

class AddCachePostsUseCaseImpl(private val postsDBRepo: PostsDBRepo) : AddCachePostsUseCase {
    override fun addPosts(posts: List<PostModel>) {
        postsDBRepo.insertAll(posts)
    }
}