package com.example.domain.usecases

import com.example.domain.model.PostModel
import com.example.domain.repository.PostsDBRepo
import io.reactivex.Flowable

class GetCachePostsUseCaseImpl(private val postsDBRepo: PostsDBRepo) : GetCachePostsUseCase {
    override fun getPostsObs(): Flowable<List<PostModel>> {
        return postsDBRepo.getAll()
    }
}