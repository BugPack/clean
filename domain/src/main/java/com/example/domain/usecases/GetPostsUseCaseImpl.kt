package com.example.domain.usecases

import com.example.domain.repository.PostsRepo

class GetPostsUseCaseImpl(private val postsRepo: PostsRepo) : GetPostsUseCase {
    override fun getPostsObs() = postsRepo.getPostsObs()
}