package com.example.domain.usecases

import com.example.domain.model.PostModel
import com.example.domain.repository.PostsDBRepo

class UpdateCachePostUseCaseImpl(private val postsDBRepo: PostsDBRepo) : UpdateCachePostUseCase {
    override fun updatePost(postModel: PostModel) {
        postsDBRepo.update(postModel)
    }
}