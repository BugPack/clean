package com.example.domain.usecases

import com.example.domain.model.PostModel

interface UpdateCachePostUseCase {
    fun updatePost(postModel: PostModel)
}