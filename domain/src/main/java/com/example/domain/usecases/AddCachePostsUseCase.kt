package com.example.domain.usecases

import com.example.domain.model.PostModel

interface AddCachePostsUseCase {
    fun addPosts(posts: List<PostModel>)
}