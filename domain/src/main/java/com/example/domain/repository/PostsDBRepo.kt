package com.example.domain.repository

import com.example.domain.model.PostModel
import io.reactivex.Flowable

interface PostsDBRepo {
    fun getAll(): Flowable<List<PostModel>>
    fun getFavorites(): Flowable<List<PostModel>>
    fun insertAll(posts: List<PostModel>)
    fun insert(post: PostModel)
    fun update(post: PostModel)
    fun delete(post: PostModel)
}