package com.example.domain.repository

import com.example.domain.model.PostModel
import io.reactivex.Observable

interface PostsRepo {
    fun getPostsObs(): Observable<List<PostModel>>
}