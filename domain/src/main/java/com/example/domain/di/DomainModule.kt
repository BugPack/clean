package com.example.domain.di

import com.example.domain.usecases.*
import org.koin.dsl.module

val domainModule = module {
    factory<GetPostsUseCase> { GetPostsUseCaseImpl(postsRepo = get()) }
    factory<UpdateCachePostUseCase> { UpdateCachePostUseCaseImpl(postsDBRepo = get()) }
    factory<GetCacheFavoritePostsUseCase> { GetCacheFavoritePostsUseCaseImpl(postsDBRepo = get()) }
    factory<AddCachePostsUseCase> { AddCachePostsUseCaseImpl(postsDBRepo = get()) }
    factory<GetCachePostsUseCase> { GetCachePostsUseCaseImpl(postsDBRepo = get()) }
}