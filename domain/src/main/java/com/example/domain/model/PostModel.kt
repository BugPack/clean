package com.example.domain.model

import java.io.Serializable

data class PostModel(
    var userId: Int,
    var id: Int,
    var title: String,
    var body: String,
    var isFavorite: Boolean = false
) : Serializable