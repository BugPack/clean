package com.example.clean_test.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.clean_test.Constants
import com.example.clean_test.R
import com.example.clean_test.databinding.FragmentPostBinding
import com.example.clean_test.viewmodel.PostViewModel
import com.example.domain.model.PostModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostFragment : Fragment() {
    companion object {
        fun getInstance(post: PostModel) = PostFragment().apply {
            arguments = Bundle().apply {
                putSerializable(Constants.POST_KEY, post)
            }
        }
    }

    private val postViewModel: PostViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(Constants.POST_KEY)) {
                val post = it.getSerializable(Constants.POST_KEY) as PostModel
                postViewModel.setPostFields(post)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentPostBinding>(
            inflater,
            R.layout.fragment_post,
            container,
            false
        )
        binding.postViewModel = postViewModel
        return binding.root
    }
}