package com.example.clean_test.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.clean_test.R
import com.example.domain.model.PostModel
import kotlinx.android.synthetic.main.list_item_post.view.*

class PostsAdapter(
    private val postsInterface: PostsInterface
) : RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {
    interface PostsInterface {
        fun onClickItem(post: PostModel)
        fun onClickFavorite(post: PostModel, isFavorite: Boolean)
    }

    private val dataset = arrayListOf<PostModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostsViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_post, parent, false)
    )

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val item = dataset[position]
        holder.apply {
            title?.text = item.title.capitalize()
            description?.text = item.body.capitalize()
            rootLayout?.setOnClickListener {
                postsInterface.onClickItem(item)
            }
            if (item.isFavorite) {
                setFavoriteState(favorite, true)
            } else {
                setFavoriteState(favorite, false)
            }
            favorite?.setOnClickListener {
                if (item.isFavorite) {
                    postsInterface.onClickFavorite(item, false)
                } else {
                    postsInterface.onClickFavorite(item, true)
                }
            }
        }
    }

    private fun setFavoriteState(imageView: ImageView, isFavorite: Boolean) {
        if (isFavorite) {
            imageView.background = ContextCompat.getDrawable(
                imageView.context,
                R.drawable.ic_star_on
            )
        } else {
            imageView.background = ContextCompat.getDrawable(
                imageView.context,
                R.drawable.ic_star_off
            )
        }
    }

    fun addItems(arrayList: ArrayList<PostModel>) {
        dataset.clear()
        dataset.addAll(arrayList)
        notifyDataSetChanged()
    }

    fun updateItem(post: PostModel) {
        dataset.find { it.id == post.id }?.let {
            it.isFavorite = post.isFavorite
            notifyItemChanged(dataset.indexOf(it))
        }
    }

    fun updateItems(arrayList: ArrayList<PostModel>) {
        arrayList.forEach { post ->
            updateItem(post)
        }
    }

    class PostsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title = itemView.tvTitle
        var description = itemView.tvDescription
        var rootLayout = itemView.rootLayout
        var favorite = itemView.ivFavorite
    }
}