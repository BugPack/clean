package com.example.clean_test.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.clean_test.R
import com.example.clean_test.viewmodel.PostsViewModel
import kotlinx.android.synthetic.main.fragment_posts.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostsFragment : Fragment(R.layout.fragment_posts) {
    companion object {
        fun getInstance() = PostsFragment()
    }

    private val postsViewModel: PostsViewModel by viewModel()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initList()
    }

    private fun initList() {
        rvList?.layoutManager = LinearLayoutManager(context)
        rvList?.adapter = postsViewModel.getAdapter()
        postsViewModel.getPosts()
    }
}