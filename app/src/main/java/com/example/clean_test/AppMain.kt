package com.example.clean_test

import android.app.Application
import com.example.clean_test.di.appModule
import com.example.data.di.databaseModule
import com.example.data.di.networkModule
import com.example.domain.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppMain : Application() {
    companion object {
        lateinit var INSTANCE: AppMain
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AppMain)
            modules(appModules + domainModules + dataModules)
        }
        INSTANCE = this
    }
}

val appModules = listOf(appModule)
val domainModules = listOf(domainModule)
val dataModules = listOf(networkModule, databaseModule)
