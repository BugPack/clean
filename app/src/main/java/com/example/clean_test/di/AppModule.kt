package com.example.clean_test.di

import com.example.clean_test.viewmodel.PostViewModel
import com.example.clean_test.viewmodel.PostsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

val appModule = module {
    single { Cicerone.create() }
    single<Router> { get<Cicerone<Router>>().router }
    single<NavigatorHolder> { get<Cicerone<Router>>().navigatorHolder }

    viewModel {
        PostsViewModel(
            getPostsUseCase = get(),
            router = get(),
            updateCachePostUseCase = get(),
            getCacheFavoritePostsUseCase = get(),
            addCachePostsUseCase = get(),
            getCachePostsUseCase = get()
        )
    }
    viewModel { PostViewModel(updateCachePostUseCase = get()) }
}