package com.example.clean_test.navigation

import androidx.fragment.app.Fragment
import com.example.clean_test.ui.fragment.PostFragment
import com.example.clean_test.ui.fragment.PostsFragment
import com.example.domain.model.PostModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

object ScreenPool {
    class PostsScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PostsFragment.getInstance()
        }
    }

    class PostScreen(private val post: PostModel) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PostFragment.getInstance(post)
        }
    }
}