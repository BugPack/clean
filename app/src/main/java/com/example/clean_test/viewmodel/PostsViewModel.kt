package com.example.clean_test.viewmodel

import androidx.lifecycle.ViewModel
import com.example.clean_test.navigation.ScreenPool
import com.example.clean_test.ui.adapter.PostsAdapter
import com.example.domain.model.PostModel
import com.example.domain.usecases.*
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router

class PostsViewModel(
    private val getPostsUseCase: GetPostsUseCase,
    private val router: Router,
    private val updateCachePostUseCase: UpdateCachePostUseCase,
    private val getCacheFavoritePostsUseCase: GetCacheFavoritePostsUseCase,
    private val addCachePostsUseCase: AddCachePostsUseCase,
    private val getCachePostsUseCase: GetCachePostsUseCase
) : ViewModel(), PostsAdapter.PostsInterface {
    private val disposables = CompositeDisposable()
    private var adapter = PostsAdapter(this)

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun getAdapter() = adapter

    fun getPosts() {
        disposables.add(
            getPostsUseCase.getPostsObs().subscribe({
                adapter.addItems(ArrayList(it))

                disposables.add(
                    getCacheFavoritePostsUseCase.getFavoritesFlow().subscribe({ favorites ->
                        adapter.updateItems(ArrayList(favorites))
                    }, {})
                )

                addCachePostsUseCase.addPosts(it)
            }, {
                disposables.addAll(
                    getCachePostsUseCase.getPostsObs().subscribe({
                        adapter.addItems(ArrayList(it))

                        disposables.add(
                            getCacheFavoritePostsUseCase.getFavoritesFlow().subscribe({ favorites ->
                                adapter.updateItems(ArrayList(favorites))
                            }, {})
                        )
                    }, {})
                )
            })
        )
    }

    override fun onClickItem(post: PostModel) {
        router.navigateTo(ScreenPool.PostScreen(post))
    }

    override fun onClickFavorite(post: PostModel, isFavorite: Boolean) {
        post.isFavorite = isFavorite
        adapter.updateItem(post)
        updateCachePostUseCase.updatePost(post)
    }
}