package com.example.clean_test.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.example.domain.model.PostModel
import com.example.domain.usecases.UpdateCachePostUseCase

class PostViewModel(
    private val updateCachePostUseCase: UpdateCachePostUseCase
) : ViewModel() {
    var userId = ObservableInt()
    var id = ObservableInt()
    var title = ObservableField<String>()
    var body = ObservableField<String>()
    var isFavorite = ObservableBoolean()

    fun setPostFields(post: PostModel) {
        userId.set(post.userId)
        id.set(post.id)
        title.set(post.title)
        body.set(post.body)
        isFavorite.set(post.isFavorite)
    }

    fun onClickFavorite() {
        val post = PostModel(
            userId = userId.get(),
            id = id.get(),
            title = title.get() ?: "",
            body = body.get() ?: "",
            isFavorite = !isFavorite.get()
        )
        isFavorite.set(!isFavorite.get())
        updateCachePostUseCase.updatePost(post)
    }
}