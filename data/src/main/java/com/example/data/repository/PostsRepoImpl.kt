package com.example.data.repository

import com.example.data.apiservice.ApiService
import com.example.domain.model.PostModel
import com.example.domain.repository.PostsRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PostsRepoImpl(
    private val apiService: ApiService
) : PostsRepo {
    override fun getPostsObs(): Observable<List<PostModel>> {
        return apiService.getPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { list ->
                Observable.fromIterable(list)
                    .map { item ->
                        PostModel(
                            userId = item.userId,
                            id = item.id,
                            title = item.title,
                            body = item.body
                        )
                    }
                    .toList()
                    .toObservable()
            }
    }
}