package com.example.data.repository

import com.example.domain.model.PostModel
import com.example.domain.repository.PostsRepo
import io.reactivex.Observable

class PostsRepoImplMock : PostsRepo {
    override fun getPostsObs(): Observable<List<PostModel>> {
        val array = arrayListOf<PostModel>()
        for (i in 0..20) {
            array.add(PostModel(userId = i, id = i, title = "title_$i", body = "body_$i"))
        }
        return Observable.fromArray(array)
    }
}