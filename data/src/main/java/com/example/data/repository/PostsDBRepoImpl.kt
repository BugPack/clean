package com.example.data.repository

import com.example.data.db.PostsDao
import com.example.data.db.model.PostEntity
import com.example.domain.model.PostModel
import com.example.domain.repository.PostsDBRepo
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PostsDBRepoImpl(private val postsDao: PostsDao) : PostsDBRepo {
    override fun getAll(): Flowable<List<PostModel>> {
        return postsDao.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { list ->
                Flowable.fromIterable(list)
                    .map { item -> convertPostEntityToPostModel(item) }
                    .toList()
                    .toFlowable()
            }
    }

    override fun getFavorites(): Flowable<List<PostModel>> {
        return postsDao.getFavorites()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { list ->
                Flowable.fromIterable(list)
                    .map { item -> convertPostEntityToPostModel(item) }
                    .toList()
                    .toFlowable()
            }
    }

    override fun insertAll(posts: List<PostModel>) {
        Observable.create<PostModel> {
            posts.forEach { post ->
                postsDao.insert(convertPostModelToPostEntity(post))
            }
            it.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun insert(post: PostModel) {
        Observable.create<PostModel> {
            postsDao.insert(convertPostModelToPostEntity(post))
            it.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun update(post: PostModel) {
        Observable.create<PostModel> {
            postsDao.update(convertPostModelToPostEntity(post))
            it.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun delete(post: PostModel) {
        Observable.create<PostModel> {
            postsDao.delete(convertPostModelToPostEntity(post))
            it.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun convertPostEntityToPostModel(postEntity: PostEntity): PostModel {
        return PostModel(
            userId = postEntity.userId,
            id = postEntity.id,
            title = postEntity.title,
            body = postEntity.body,
            isFavorite = postEntity.isFavorite
        )
    }

    private fun convertPostModelToPostEntity(postModel: PostModel): PostEntity {
        return PostEntity(
            userId = postModel.userId,
            id = postModel.id,
            title = postModel.title,
            body = postModel.body,
            isFavorite = postModel.isFavorite
        )
    }
}