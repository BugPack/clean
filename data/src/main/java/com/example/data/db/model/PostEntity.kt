package com.example.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PostEntity(
    var userId: Int,
    @PrimaryKey
    var id: Int,
    var title: String,
    var body: String,
    var isFavorite: Boolean = false
)