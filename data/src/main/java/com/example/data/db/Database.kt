package com.example.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.db.model.PostEntity

@Database(entities = [PostEntity::class], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract fun postsDao(): PostsDao
}