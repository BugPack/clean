package com.example.data.db

import androidx.room.*
import com.example.data.db.model.PostEntity
import io.reactivex.Flowable

@Dao
interface PostsDao {
    @Query("SELECT * FROM postentity")
    fun getAll(): Flowable<List<PostEntity>>

    @Query("SELECT * FROM postentity where isFavorite = 1")
    fun getFavorites(): Flowable<List<PostEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(post: PostEntity)

    @Update
    fun update(post: PostEntity)

    @Delete
    fun delete(post: PostEntity)
}