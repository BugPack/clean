package com.example.data.di

import androidx.room.Room
import com.example.data.db.Database
import com.example.data.repository.PostsDBRepoImpl
import com.example.data.repository.PostsRepoImpl
import com.example.data.repository.PostsRepoImplMock
import com.example.domain.repository.PostsDBRepo
import com.example.domain.repository.PostsRepo
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(androidContext(), Database::class.java, "postsDatabase")
            .fallbackToDestructiveMigration()
            .build()
    }
    single { get<Database>().postsDao() }

    factory<PostsRepo> { PostsRepoImpl(apiService = get()) }
//    factory<PostsRepo> { PostsRepoImplMock() }

    factory<PostsDBRepo> { PostsDBRepoImpl(postsDao = get()) }
}