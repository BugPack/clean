package com.example.data.apiservice

import com.example.data.model.DataPostModel
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {
    @GET("posts")
    fun getPosts(): Observable<List<DataPostModel>>
}